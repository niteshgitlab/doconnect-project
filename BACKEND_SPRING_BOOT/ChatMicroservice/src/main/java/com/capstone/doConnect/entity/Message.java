package com.capstone.doConnect.entity;

import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "To which user u want to send")
	private String message;
	
	private String fromUser;

	
	
}
