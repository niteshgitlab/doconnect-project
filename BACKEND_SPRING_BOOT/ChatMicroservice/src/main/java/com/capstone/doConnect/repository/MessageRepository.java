package com.capstone.doConnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capstone.doConnect.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long>
{

}
