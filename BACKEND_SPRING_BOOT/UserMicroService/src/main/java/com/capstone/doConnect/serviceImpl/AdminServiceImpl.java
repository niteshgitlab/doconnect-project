package com.capstone.doConnect.serviceImpl;

import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capstone.doConnect.DTO.DeleteUserDTO;
import com.capstone.doConnect.entity.Admin;
import com.capstone.doConnect.entity.User;
import com.capstone.doConnect.exception.AlreadyThere;
import com.capstone.doConnect.exception.NotFound;
import com.capstone.doConnect.repository.AdminRepository;
import com.capstone.doConnect.repository.UserRepository;
import com.capstone.doConnect.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService
{
	@Autowired
	private AdminRepository adminRepository;
	@Autowired
	private UserRepository userRepo;
	@Override
	public Admin adminLogin(String email, String password)
	{
		Admin admin = adminRepository.findByEmail(email);
		if (Objects.isNull(admin))
			throw new NotFound();

		if (admin.getPassword().equals(password))
		{
			admin.setIsActive(true);
			adminRepository.save(admin);
		}
		else
			throw new NotFound();
		return admin;
	}

	@Override
	public String adminLogout(Long adminId)
	{
		Admin admin = adminRepository.findById(adminId).orElseThrow(() -> new NotFound("Admin not found"));
		admin.setIsActive(false);
		adminRepository.save(admin);
		return "Logged Out";
	}

	@Override
	public Admin adminRegister(Admin admin)
	{
		Admin admin1 = adminRepository.findByEmail(admin.getEmail());
		if (Objects.isNull(admin1))
			return adminRepository.save(admin);

		throw new AlreadyThere();
	}

	@Override
	public User getUser(String email)
	{
		return userRepo.findByEmail(email);
	}

	@Override
	public List<User> getAllUser()
	{
		return userRepo.findAll();
	}

	@Override
	public DeleteUserDTO deleteUser(Long answerId) {
		DeleteUserDTO deleteUserDTO = new DeleteUserDTO();

		User user = userRepo.findById(answerId).orElseThrow(() -> new NotFound("User not found"));

		userRepo.delete(user);
		deleteUserDTO.setMsg("User Removed");
		return deleteUserDTO;
	}
}
