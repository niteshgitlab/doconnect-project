package com.capstone.doConnect.serviceImpl;

import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capstone.doConnect.entity.User;
import com.capstone.doConnect.exception.AlreadyThere;
import com.capstone.doConnect.exception.NotFound;
import com.capstone.doConnect.repository.UserRepository;
import com.capstone.doConnect.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepo;
	@Override
	public User userLogin(String email, String password) {
		User user = userRepo.findByEmail(email);
		if (Objects.isNull(user))
			throw new NotFound();

		if (user.getPassword().equals(password)) {
			user.setIsActive(true);
			userRepo.save(user);
		} else
			throw new NotFound();
		return user;
	}

	@Override
	public String userLogout(Long userId) {
		User user = userRepo.findById(userId).orElseThrow(() -> new NotFound("User Not Found" + userId));
		user.setIsActive(false);
		userRepo.save(user);
		return "Logged Out";
	}

	@Override
	public User userRegister(User user) {
		User user1 = userRepo.findByEmail(user.getEmail());
		if (Objects.isNull(user1))
			return userRepo.save(user);
		throw new AlreadyThere();
	}

}
