package com.wipro.doconnect.entity;

import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Answer
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message = "mention the answer")
	private String answer;
	
	@OneToOne
	private User answerUser;
	
	@OneToOne
	private Question question;
	private Boolean isApproved = false;
	
}
