package com.wipro.doconnect.entity;

import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Question
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message = "Question can not be blank")
	private String question;
	
	@OneToOne
	private User user;
	
	@NotBlank(message = "provide the topic")
	private String topic;
	
	private Boolean isApproved = false;

	
	
}
